package com.sampleCarrierGeneratorPortalApplication.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SampleCarrierGeneratorPortalApplicationServiceAsync {
  void getMessage(String msg, AsyncCallback<String> async);
}
