package com.sampleCarrierGeneratorPortalApplication.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("SampleCarrierGeneratorPortalApplicationService")
public interface SampleCarrierGeneratorPortalApplicationService extends RemoteService {
  // Sample interface method of remote interface
  String getMessage(String msg);

  /**
   * Utility/Convenience class.
   * Use SampleCarrierGeneratorPortalApplicationService.App.getInstance() to access static instance of SampleCarrierGeneratorPortalApplicationServiceAsync
   */
  public static class App {
    private static SampleCarrierGeneratorPortalApplicationServiceAsync ourInstance = GWT.create(SampleCarrierGeneratorPortalApplicationService.class);

    public static synchronized SampleCarrierGeneratorPortalApplicationServiceAsync getInstance() {
      return ourInstance;
    }
  }
}
